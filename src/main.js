import Vue from "vue";
import Vuelidate from "vuelidate";
import App from "./App.vue";
import Loader from "@/components/app/Loader";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import dateFilter from "@/filters/date.filer.js";
import messagePlugin from "@/utils/message.plugin";
import "materialize-css/dist/js/materialize.min";

import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";

Vue.config.productionTip = false;

Vue.use(messagePlugin);
Vue.use(Vuelidate);
Vue.filter("date", dateFilter);
Vue.component("Loader", Loader);

firebase.initializeApp({
  apiKey: "AIzaSyDCKWuaYyxthTvYX3ffTI--chR2B9YgC90",
  authDomain: "bro-crm.firebaseapp.com",
  databaseURL: "https://bro-crm-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "bro-crm",
  storageBucket: "bro-crm.appspot.com",
  messagingSenderId: "72666629024",
  appId: "1:72666629024:web:93e87bec3a936866015ac5",
});

let app;
firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      router,
      store,
      render: (h) => h(App),
    }).$mount("#app");
  }
});
